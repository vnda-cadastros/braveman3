## Orientações de cadastro do topo

#### Menu principal
Menu; posição **principal**

*Obs: fica do lado esquerdo do logo*

- - - - - - -

#### Menu institucional
Menu; posição **institucional**

*Obs: fica do lado direito do logo*

- - - - - - -

#### Banner do topo
Banner; posição **header-topo**

- Texto no campo descrição
- Cor de fundo dinamica pelo campo cor do banner (padrão é cinza claro)
- Cor do texto dinamica pelo campo subtítulo do banner (padrão é preto)

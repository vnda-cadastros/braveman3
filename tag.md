## Orientações de cadastro para a Tag

**Imagem do topo**
Banner; 1920x400; posição: imagem-topo-{ campo name da tag }

**Tag flag nos produtos**
Tag; tipo: flag

- Aceita imagem pelo campo imagem da tag
- Se não houver imagem, exibe o campo título da tag, podendo ter a cor de fundo alterada colocando o hexadecimal no campo subtitulo da tag.


- - - - - - -

**Filtros**

- Categoria: Tag do tipo categoria. Esse filtro não aparece se a página atual for uma tag do tipo categoria

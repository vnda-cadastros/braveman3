## Orientações de cadastro Home

#### Banner principal (fullbanner)

Banner; 1920x800; posição: fullbanner

- Texto do banner no campo descrição do banner

**Mobile**
Banner; 768x1000; posição: fullbanner-mobile

- Texto do banner no campo descrição do banner

- - - - - - -


#### Carrossel de produtos

Tag; produtos-home

- Título da sessão dinamica pelo título da tag
- Subtítulo da sessão dinamica pelo subtítulo da tag
- Tag do tipo categoria para a informação acima do nome de produto
- Tag do tipo marca para a informação acima do nome de produto

- - - - - - -


#### Vídeo na home

Banner; posição: home-video

- Colocar id do vídeo no campo subtítulo.
- Vídeo deve ser do vimeo


- - - - - - -


#### Banners de apoio

Banner; 1000x666;posição: home-banners-apoio

- Título do banner (antes da imagem) dinamico pelo campo subtítulo do banner
- Texto do banner (sobre a imagem) dinamico pelo campo descrição do banner


- - - - - - -


#### Instagram

Banner; posição: token-instagram

- Colocar token do instagram no campo subtítulo do banner
- Título da sessão dinamico pelo campo descrição do banner

![486x633](uploads/3b996ed49e9e6a091ba32b28c8323379/486x633.png)
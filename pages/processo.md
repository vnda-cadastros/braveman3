## Orientações de cadastro da página Processo

Link: https://braveman.vnda.com.br/p/processo

**Topo**

- Dinamico com Banners
- Posição: *page-processo-topo*
- Dimensão das imagens: 1920x800
- Título dinamizado com o campo *subtítulo* do banner
- Descrição dinamizado com o campo *descrição* do banner


- - - - - - -

**Vídeo**

- Dinamico com Banners
- Posição: *processo-video*
- Colocar id do vídeo no campo subtítulo.
- Vídeo deve ser do vimeo


- - - - - - -

**Sessão de imagens com texto**

- Dinamico com Banners
- Posição: *page-processo-conteudo*
- Dimensão das imagens: 1920x960
- Título do banner dinamizado com o campo *subtítulo* do banner
- Texto do banner dinamizado com o campo *descrição* do banner

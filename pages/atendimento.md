## Orientações de cadastro da página Atendimento

Link: https://braveman.vnda.com.br/p/atendimento

- - - - - - -

**Informações de contato**

- Banner com posição *page-banner-atendimento*
- Imagem com dimensão livre
- O campo subtítulo do banner define o texto acima do banner. Exemplo: `{{título}} | {{subtítulo}} ou somente {{título}}`
- texto do box: Campo descrição do banner.
- Imagem:  uma imagem pequena opcional que fica acima do box como na referencia, ideal para o logo.


**Abas**

- Banners com a posição *atendimento-item*, não há limite para os banners;
- Campo Subtítulo: o texto do accordion fechado
- Campo Description: o conteúdo do accordion aberto

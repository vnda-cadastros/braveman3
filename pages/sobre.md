## Orientações de cadastro da página Sobre

Link: http://braveman.vnda.com.br/p/sobre


**Topo**

- Dinamico com Banners
- Posição: *page-sobre-topo*
- Dimensão das imagens: 1920x800
- Título dinamizado com o campo *subtítulo* do banner
- Descrição dinamizado com o campo *descrição* do banner


- - - - - - -

**Sessão de imagens com texto**

- Dinamico com Banners
- Posição: *page-sobre-conteudo*
- Dimensão das imagens: 1920x960
- Título do banner dinamizado com o campo *subtítulo* do banner
- Texto do banner dinamizado com o campo *descrição* do banner

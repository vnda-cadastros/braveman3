## Orientações de cadastro página de produto

Produtos exemplos:
https://braveman.vnda.com.br/produto/cor-e-tamanho-2
https://braveman.vnda.com.br/produto/produto-teste-3-c-85
https://braveman.vnda.com.br/produto/produto-teste-2-aa-84
https://braveman.vnda.com.br/produto/produto-teste-1-a-83

- - - -


**Banners de produto**
Banner; 1920xlivre; posição: produto-{ referencia do produto }

- - - -


**Vídeo**
Dinamizado pelos vídeos de produto

- - - -


## Atributos

- Todos os atributos de produtos são dinamicos para facilitar na escolha de gravações, cores, tamanhos e complementos.


**Gravação**
Indicar qual atributo é referente à gravação

Ex: https://braveman.vnda.com.br/produto/produto-teste-1-a-83
Ex: https://braveman.vnda.com.br/admin/produtos/editar?id=83

Campo extra: http://braveman.vnda.com.br/produto/produto-teste-2-aa-84

- Quando o atributo 1 indicar a gravação, colocar no produto tag do tipo *gravacao_atributo1*
- Quando o atributo 2 indicar a gravação, colocar no produto tag do tipo *gravacao_atributo2*
- Quando o atributo 3 indicar a gravação, colocar no produto tag do tipo *gravacao_atributo3*
- Quando a gravação não varia preço e vale para todos os skus, pode ser usada a tag do tipo *gravacao_extra* para a gravação como campo extra
- Título da gravação dinamico pelo campo título da tag


- - - - -

**Cores/modelos**
Indicar qual atributo é referente às cores/modelos

Ex: http://braveman.vnda.com.br/produto/produto-teste-2-aa-84
Ex: https://braveman.vnda.com.br/admin/produtos/editar?id=84

- Quando o atributo 1 indicar a cor, colocar no produto tag do tipo *cor_atributo1*
- Quando o atributo 2 indicar a cor, colocar no produto tag do tipo *cor_atributo2*
- Quando o atributo 3 indicar a cor, colocar no produto tag do tipo *cor_atributo3*
- Título da cor/modelo dinamico pelo campo título da tag
- Cadastro da cor deve seguir como ex: *Branco | #ffffff*, sendo o hexa primeiro e o título da cor em segundo


- - - - -

**Tamanhos**
Indicar qual atributo é referente aos tamanhos

Ex: http://braveman.vnda.com.br/produto/produto-teste-2-aa-84
Ex: https://braveman.vnda.com.br/admin/produtos/editar?id=84

- Quando o atributo 1 indicar o tamanho, colocar no produto tag do tipo *tamanho_atributo1*
- Quando o atributo 2 indicar o tamanho, colocar no produto tag do tipo *tamanho_atributo2*
- Quando o atributo 3 indicar o tamanho, colocar no produto tag do tipo *tamanho_atributo3*
- Título do tamanho dinamico pelo campo título da tag


- - - - -

**Complementos**
Indicar qual atributo é referente aos complementos

Obs: Complementos podem ser usados para toda variação de texto

Ex: https://braveman.vnda.com.br/produto/produto-teste-3-c-85
Ex: https://braveman.vnda.com.br/admin/produtos/editar?id=85

- Quando o atributo 1 indicar o complemento, colocar no produto tag do tipo *complemento_atributo1*
- Quando o atributo 2 indicar o complemento, colocar no produto tag do tipo *complemento_atributo2*
- Quando o atributo 3 indicar o complemento, colocar no produto tag do tipo *complemento_atributo3*
- Título do complemento dinamico pelo campo título da tag


- - - - -

**Produtos Relacionados**
Produtos que são relacionados com o atual
Tag; tipo: relacionado

- Limite de 12 produtos no carrossel


- - - - -

**Descrições**

Os produtos podem ter descrições curtas e descrições detalhadas

- Descrição curta fica abaixo do preço
- Descrições detalhadas ficam em tabs abaixo da primeira sessão do produto
- Separar as partes da descrição por `- - - - -`, primeira parte fica sempre como descrição curta, demais partes ficam como abas na sessão de descrição detalhada, marcar título da tab com `##`. Ex:

```

Descrição curta do produto. Adipisicing occaecat deserunt duis consectetur consectetur excepteur velit.

- - - - - -

## Título da tab 1

Veniam nisi ex velit proident ea minim reprehenderit ad duis minim ut ut commodo. Minim commodo cillum ullamco irure proident amet et cillum eiusmod proident culpa duis ad excepteur. Incididunt sint nulla dolor laborum irure elit ullamco ut. Enim quis tempor elit incididunt dolore Lorem ex consequat ipsum aliqua nulla.

- - - - - -

## Título da tab 2

Quis cupidatat exercitation consequat laboris sunt sint nulla. Cupidatat veniam sint ipsum aliquip. Qui enim do qui fugiat quis laborum enim. Nulla nulla duis ad aute sint occaecat amet et velit mollit do ullamco dolore et. Reprehenderit excepteur qui non nisi cupidatat ullamco dolor nostrud deserunt. Proident amet id duis sit Lorem cupidatat pariatur velit minim in.

- - - - - -

## Título da tab 3

Mollit do Lorem officia proident velit aliquip minim.

- Ipsum Lorem tempor in dolor velit mollit irure deserunt cupidatat incididunt tempor tempor.
- Consectetur nostrud in ullamco aliquip voluptate adipisicing.
- Incididunt et non reprehenderit irure non laboris in duis.
- Aute excepteur duis et et consectetur sint.

```

